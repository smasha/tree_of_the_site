﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace Tree_of_the_site
{
    class TreeNode
    {
        public string Url;
        public TreeNode Parent;
        public List<TreeNode> Children;       
    }

    class Page
    {
        string content;

        public static Page Load(string url)
        {           
            WebClient client = new WebClient();
            Stream data = client.OpenRead(url);
            StreamReader reader = new StreamReader(data, Encoding.GetEncoding(1251));
            var page = new Page();
            page.content = reader.ReadToEnd();
            return page;
        }

        public List<string> FindLinks()
        {
            var links = new List<string>();
            Match m;         
            string HRefPattern = @"(?i)<\s*?a\s+[\S\s\x22\x27\x3d]*?href=[\x22\x27]?([^\s\x22\x27<>]+)[\x22\x27]?.*?>";
            m = Regex.Match(content, HRefPattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
            while (m.Success)
            {
                links.Add(m.Groups[1].Value);                           
                m = m.NextMatch();
            }                     
            return links;
        }
    }

    class Program
    {
        static string GetHostFromUrl(string url)
        {
            string inv;
            int index = url.IndexOf("//");
            inv = url.Substring(index + 2);           
            int indexEnd = inv.IndexOf('/');
            if (indexEnd > 0)
            {
                inv = inv.Substring(0, indexEnd);
            }           
            return inv;
        }

        static void BuildTree(string rootHost, TreeNode parent, List<string> allLinks)
        {
            var pageUrl = parent.Url.StartsWith("/") ? "http://" + rootHost + parent.Url : parent.Url;
            if (!pageUrl.StartsWith("http")) return;
            Page page = Page.Load(pageUrl);
            List<string> links = page.FindLinks();
            foreach (string link in links)
            {
                if (allLinks.Contains(link))
                {
                    continue;
                }
                else
                {
                    allLinks.Add(link);
                }
                string host = GetHostFromUrl(link);
                TreeNode node = new TreeNode();
                node.Url = link;
                node.Children = new List<TreeNode>();
                if (host == rootHost || host == "")
                {
                    BuildTree(rootHost, node, allLinks);
                }
                parent.Children.Add(node);                
            }
        }       

        static void Main(string[] args)
        {
            TreeNode tree = new TreeNode();
            tree.Children = new List<TreeNode>();
            tree.Url = "http://www.itstep.by";
            string host = GetHostFromUrl(tree.Url);
            BuildTree(host, tree, new List<string>());    
                     
        }
    }
}
